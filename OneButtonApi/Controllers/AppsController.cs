﻿using System;
using System.Dynamic;
using System.Globalization;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Threading;
using System.Linq;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Web;
using System.Net;
using System.Text;
using System.Net.Http;
using System.Web.Http;
using System.Web.Security;
using System.Web.Http.Cors;
using System.Net.Http.Headers;
using System.Web.Helpers;
using Newtonsoft.Json;
using OneButtonApi.Models;
using BLL.Dao;
using BLL.Entities;
using BLL.Common;
using Newtonsoft.Json.Linq;

namespace OneButtonApi.Controllers
{
    /// <summary>
    /// This API allows a user to play with their camera's .apks
    /// </summary>
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AppsController : ApiController
    {
        /// <summary>
        /// Get user's app details
        /// </summary>
        /// <param name="code">App code</param>
        /// <returns>See sample response data</returns>
        [Route("v1/apps/{code}")]
        [HttpGet]
        public AppInfoModel Get(string code)
        {
            return AppModel.Convert(AppDao.Get(code));
        }

        /// <summary>
        /// Get oldest apps with '0=New' status
        /// </summary>
        /// <param name="count">Number of apps to fetch from list</param>
        /// <returns>Returns list of apps</returns>
        [Route("v1/apps/{count:int}")]
        [HttpGet]
        public IQueryable<AppInfoModel> GetTop(int count)
        {
            return AppModel.Convert(AppDao.GetList(count, AppStatus.New)).AsQueryable();
        }

        /// <summary>
        /// Get number of apps (count) with given status
        /// </summary>
        /// <param name="count">Number of apps to fetch from list</param>
        /// <param name="status">App status</param>
        /// <returns>Returns list of apps</returns>
        [Route("v1/apps/{count:int}/{status:int}")]
        [HttpGet]
        public IQueryable<AppInfoModel> GetTop(int count, int status)
        {
            return AppModel.Convert(AppDao.GetList(count, (AppStatus)status)).AsQueryable();
        }

        /// <summary>
        /// Get list of apps of all users
        /// </summary>
        /// <returns>See sample response data</returns>
        [Route("v1/apps/users")]
        [HttpGet]
        public IQueryable<AppInfoModel> GetAll()
        {
            return AppModel.Convert(AppDao.GetList(null)).AsQueryable();
        }

        /// <summary>
        /// Get list of user's apps
        /// </summary>
        /// <param name="id">Evercam User Id</param>
        /// <returns>See sample response data</returns>
        [Route("v1/apps/users/{id}")]
        [HttpGet]
        public IQueryable<AppInfoModel> GetByUser(string id)
        {
            return AppModel.Convert(AppDao.GetListByUserId(id, null)).AsQueryable();
        }

        /// <summary>
        /// Creates an App
        /// </summary>
        /// <param name="data">See sample request data below</param>
        /// <returns>Returns complete app details (see sample response data below)</returns>
        [Route("v1/apps")]
        [HttpPost]
        public AppInfoModel Post([FromBody]AppModel data)
        {
            App app = AppModel.Convert(data);
            int tid = 0;
            try
            {
                string title = data.title.Replace(" ", "_").Trim();
                string tempPath = HttpContext.Current.Server.MapPath("~/temp/");
                string appsPath = Path.Combine(Settings.AppFilePath, title);
                string downloadPath = Path.Combine(Settings.DownloadFilePath, app.Code);

                if (!Directory.Exists(tempPath)) Directory.CreateDirectory(tempPath);
                if (!Directory.Exists(appsPath)) Directory.CreateDirectory(appsPath);
                if (!Directory.Exists(downloadPath)) Directory.CreateDirectory(downloadPath);

                string imageFile = HttpContext.Current.Server.UrlDecode(data.image_file);
                string tempImage = "";

                if (string.IsNullOrEmpty(imageFile))
                {
                    SnapshotData snap = BLL.Common.Utils.DownloadImage(data.camera_url, data.camera_user, data.camera_password, true);
                    if (snap.Data.Length > 0 && snap.ContentType.Contains("image"))
                        tempImage = new WebImage(snap.Data).Save(Path.Combine(tempPath, app.Code + ".jpg")).FileName;
                    else
                        throw new HttpResponseException(new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new StringContent("Could not download image from camera") });
                }
                else
                {
                    SnapshotData snap = BLL.Common.Utils.DownloadImage(imageFile, "", "", false);
                    if (snap.Data.Length > 0 && snap.ContentType.Contains("image"))
                        tempImage = SaveAsJpeg(snap, Path.Combine(tempPath, app.Code + ".jpg"));
                    else
                        throw new HttpResponseException(new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new StringContent("Could not access the uploaded image") });
                }

                string tempImage2 = Utils.MakeSquareImage(tempImage);

                string tempThumb = new WebImage(tempImage2)
                    .Resize(140, 140, false, true)
                    .Crop(1, 1)
                    .Save(Path.Combine(tempPath, app.Code + ".png"), "png").FileName;

                string thumbFile = Path.Combine(downloadPath, app.Code + ".png");
                Image imgThumb = BLL.Common.Utils.MakeIconImage(tempThumb, 10);
                imgThumb.Save(thumbFile, System.Drawing.Imaging.ImageFormat.Png);
                File.Copy(thumbFile, Path.Combine(appsPath, "ic_launcher.png"), true);

                app.IconFile = app.ThumbFile = Common.DOWNLOAD_URL + app.Code + "/" + app.Code + ".png";

                imgThumb.Dispose();
                File.Delete(tempImage);
                File.Delete(tempImage2);

                if ((tid = AppDao.Insert(app)) == 0)
                    throw new HttpResponseException(HttpStatusCode.BadRequest);

                return AppModel.Convert(AppDao.Get(tid));
            }
            catch (Exception x) {
                Utils.AppLog("Error AppsController > Post: ", x);
                throw new HttpResponseException(new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError, Content = new StringContent(x.Message) });
            }
        }

        /// <summary>
        /// Updates an App
        /// </summary>
        /// <param name="data">See sample request data below</param>
        /// <returns>Returns complete app details (see sample response data below)</returns>
        [Route("v1/apps")]
        [HttpPut]
        public AppInfoModel Put([FromBody]AppUpdateModel data)
        {
            App t = AppModel.Convert(data);
            if (!AppDao.Update(t))
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            return AppModel.Convert(AppDao.Get(t.Code));
        }

        /// <summary>
        /// Updates an App's status
        /// </summary>
        /// <param name="code">App code</param>
        /// <param name="status">0=New, 1=Processing, 2=Completed, 3=Failed</param>
        /// <returns>Returns complete app details (see sample response data below)</returns>
        [Route("v1/apps/{code}/{status:int}")]
        [HttpPut]
        public AppInfoModel Put(string code, int status)
        {
            if (!AppDao.UpdateStatus(code, status))
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            return AppModel.Convert(AppDao.Get(code));
        }

        /// <summary>
        /// Deletes an app
        /// </summary>
        /// <param name="code">App Code</param>
        /// <returns>Returns HTTP status 200/OK in case of success or HTTP error 400/BadRequest</returns>
        [Route("v1/apps/{code}")]
        [HttpDelete]
        public HttpResponseMessage Delete(string code)
        {
            App app = AppDao.Get(code);
            if (!AppDao.Delete(code))
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            string message = "";
            try
            {
                string downloadPath = Path.Combine(Settings.DownloadFilePath, app.Code);
                new DirectoryInfo(downloadPath).Delete(true);
            }
            catch (Exception x) {
                message = x.Message;
            }
            return Common.Utility.GetResponseMessage(message, HttpStatusCode.OK);
        }

        #region PRIVATE

        private string SaveAsJpeg(SnapshotData snapshot, string file)
        {
            using (Image image = Image.FromStream(new MemoryStream(snapshot.Data)))
            {
                if (snapshot.ContentType.Contains("image/jpeg"))
                {
                    image.Save(file);
                    return file;
                }
                using (Bitmap b = new Bitmap(image.Width, image.Height))
                {
                    using (Graphics g = Graphics.FromImage(b))
                    {
                        g.Clear(Color.White);
                        g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                        g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                        g.DrawImageUnscaled(image, 0, 0);
                    }
                    b.Save(file);
                }
            }

            return file;
        }

        #endregion

        #region Utils

        /// <summary>
        /// Get Evercam token details from given token endpoint data
        /// </summary>
        /// <param name="data">See sample request data below</param>
        /// <returns>See sample response data below</returns>
        [Route("v1/tokeninfo")]
        [HttpPost]
        public TokenUserModel GetTokenUser([FromBody]TokenUrlModel data)
        {
            try
            {
                data.token_endpoint = data.token_endpoint.Replace("dashboard", "api");
                string result;
                WebRequest r = WebRequest.Create(data.token_endpoint);
                r.Method = "GET";
                using (var response = (HttpWebResponse)r.GetResponse())
                {
                    result = new StreamReader(response.GetResponseStream()).ReadToEnd();
                    response.Close();
                }
                return JsonConvert.DeserializeObject<TokenUserModel>(result);
            }
            catch (Exception x) { throw new HttpResponseException(HttpStatusCode.InternalServerError); }
        }

        #endregion
    }
}
