﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BLL.Common;
using BLL.Entities;
using System.Web.Http;
using System.Net;

namespace OneButtonApi.Models
{
    public class AppModel
    {
        /// <example>evercam-camera-id</example>
        public string camera_id { get; set; }
        /// <example>http://{camera-snapshot-url}</example>
        public string camera_url { get; set; }
        /// <example>admin</example>
        public string camera_user { get; set; }
        /// <example>12345</example>
        public string camera_password { get; set; }
        /// <example>evercam-user-id</example>
        public string user_id { get; set; }
        /// <example>a1s2d3f4g5</example>
        public string title { get; set; }
        /// <example>mail@website.com</example>
        public string email { get; set; }
        /// <example>http://{server}/{filename}</example>
        public string image_file { get; set; }

        public static App Convert(AppModel model)
        {
            App app = new App();

            app.CameraId = model.camera_id;
            app.Code = Utils.GeneratePassCode(10);
            app.UserId = model.user_id;
            app.Title = model.title;
            app.ThumbFile = model.image_file;
            app.Email = model.email;
            app.CameraUrl = model.camera_url;
            app.CameraUser = model.camera_user;
            app.CameraPassword = model.camera_password;

            return app;
        }

        public static App Convert(AppUpdateModel model)
        {
            App app = new App();

            app.Code = model.code;
            app.CameraId = model.camera_id;
            app.UserId = model.user_id;
            app.Title = model.title;
            app.ThumbFile = model.image_file;
            app.Email = model.email;
            app.CameraUrl = model.camera_url;
            app.CameraUser = model.camera_user;
            app.CameraPassword = model.camera_password;

            return app;
        }

        public static AppInfoModel Convert(App app)
        {
            AppInfoModel model = new AppInfoModel();

            model.id = app.ID;
            model.camera_id = app.CameraId;
            model.camera_url = app.CameraUrl;
            model.camera_user = app.CameraUser;
            model.camera_password = app.CameraPassword;
            model.user_id = app.UserId;
            model.code = app.Code;
            model.title = app.Title;
            model.thumb_file = app.ThumbFile;
            model.icon_file = app.IconFile;
            model.app_url = app.AppFile;
            model.email = app.Email;
            model.status = app.Status;
            model.is_deleted= app.IsDeleted;
            model.created_at = app.CreatedAt.ToString();
            model.modified_at = app.ModifiedAt.ToString();

            return model;
        }

        public static List<AppInfoModel> Convert(List<App> apps)
        {
            List<AppInfoModel> list = new List<AppInfoModel>();

            foreach (App app in apps)
            {
                AppInfoModel model = new AppInfoModel();

                model.id = app.ID;
                model.camera_id = app.CameraId;
                model.camera_url = app.CameraUrl;
                model.camera_user = app.CameraUser;
                model.camera_password = app.CameraPassword;
                model.user_id = app.UserId;
                model.code = app.Code;
                model.title = app.Title;
                model.thumb_file = app.ThumbFile;
                model.icon_file = app.IconFile;
                model.app_url = app.AppFile;
                model.email = app.Email;
                model.status = app.Status;
                model.is_deleted = app.IsDeleted;
                model.created_at = app.CreatedAt.ToString();
                model.modified_at = app.ModifiedAt.ToString();

                list.Add(model);
            }
            return list;
        }
    }

    public class AppInfoModel
    {
        public int id { get; set; }
        /// <example>evercam-camera-id</example>
        public string camera_id { get; set; }
        /// <example>http://{camera-snapshot-url}</example>
        public string camera_url { get; set; }
        /// <example>admin</example>
        public string camera_user { get; set; }
        /// <example>12345</example>
        public string camera_password { get; set; }
        /// <example>evercam-user-id</example>
        public string user_id { get; set; }
        /// <example>a1s2d3f4g5</example>
        public string code { get; set; }
        /// <example>my test timelapse</example>
        public string title { get; set; }
        /// <example>mail@website.com</example>
        public string email { get; set; }
        public string thumb_file { get; set; }
        /// <example>http://{server}/{filename}</example>
        public string icon_file { get; set; }
        public string app_url { get; set; }
        public int status { get; set; }
        public bool is_deleted { get; set; }
        /// <example>05/10/2013 00:00:10</example>
        public string created_at { get; set; }
        /// <example>05/10/2013 00:00:10</example>
        public string modified_at { get; set; }
    }

    public class AppUpdateModel
    {
        /// <example>a1s2d3f4g5</example>
        public string code { get; set; }
        /// <example>evercam-camera-id</example>
        public string camera_id { get; set; }
        /// <example>http://{camera-snapshot-url}</example>
        public string camera_url { get; set; }
        /// <example>admin</example>
        public string camera_user { get; set; }
        /// <example>12345</example>
        public string camera_password { get; set; }
        /// <example>evercam-user-id</example>
        public string user_id { get; set; }
        /// <example>a1s2d3f4g5</example>
        public string title { get; set; }
        /// <example>mail@website.com</example>
        public string email { get; set; }
        /// <example>http://{server}/{filename}</example>
        public string image_file { get; set; }
    }
}