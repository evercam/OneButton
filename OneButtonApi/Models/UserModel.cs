﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OneButtonApi.Models
{
    public class UserModel
    {
        /// <example>joeyb</example>
        public string id { get; set; }
        /// <example>joey</example>
        public string forename { get; set; }
        /// <example>b</example>
        public string lastname { get; set; }
        /// <example>joeyb</example>
        public string username { get; set; }
        /// <example>joeyb@email.com</example>
        public string email { get; set; }
        /// <example>us</example>
        public string country { get; set; }
        /// <example>1388042081</example>
        public string created_at { get; set; }
        /// <example>1388042081</example>
        public string updated_at { get; set; }
        /// <example>1388042081</example>
        public string confirmed_at { get; set; }
    }
}