﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OneButtonApi.Models
{
    public class CameraModel
    {
        public string id { get; set; }
        public string name { get; set; }
        public string owner { get; set; }
        public string vendor { get; set; }
        public string model { get; set; }
        public string timezone { get; set; }
        public bool is_online { get; set; }
        public bool is_public { get; set; }
        public string external_url { get; set; }
        public string internal_url { get; set; }
        public string jpg_url { get; set; }
        public string cam_username { get; set; }
        public string cam_password { get; set; }
        public string mac_address { get; set; }
        public string location { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public string last_polled_at { get; set; }
    }
}