﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneButtonAppService
{
    public class AppInfoModel
    {
        public int id { get; set; }
        public string camera_id { get; set; }
        public string camera_url { get; set; }
        public string camera_user { get; set; }
        public string camera_password { get; set; }
        public string user_id { get; set; }
        public string code { get; set; }
        public string title { get; set; }
        public string email { get; set; }
        public string thumb_file { get; set; }
        public string icon_file { get; set; }
        public string app_url { get; set; }
        public int status { get; set; }
        public bool is_deleted { get; set; }
        public string created_at { get; set; }
        public string modified_at { get; set; }
    }

    public class AppUpdateModel
    {
        public string code { get; set; }
        public string camera_id { get; set; }
        public string camera_url { get; set; }
        public string camera_user { get; set; }
        public string camera_password { get; set; }
        public string user_id { get; set; }
        public string title { get; set; }
        public string email { get; set; }
        public string image_file { get; set; }
    }

    public class AppStatusUpdateModel
    {
        public int status { get; set; }
        public string app_url { get; set; }
    }
}
