﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using System.Configuration;
using System.Collections;
using System.Diagnostics;
using System.Net.Mail;
using System.Net;
using BLL.Dao;
using BLL.Entities;
using BLL.Common;

namespace OneButtonAppService
{
    public class Executor
    {
        bool isServiceRunning = true;

        public void Execute()
        {
            Utils.FileLog("Service execution started..." + Environment.NewLine);
            int status = 0; // new

            while (isServiceRunning)
            {
                status = 0;
                Utils.FileLog("Fetching pending apps...");
                List<App> apps = AppDao.GetList(1, AppStatus.New);
                
                /// if there is no new app, then pick any of aborted app 
                /// (may be started processing but aborted for some reason)
                if (apps.Count == 0)
                    apps = AppDao.GetList(1, AppStatus.Processing);

                if (apps.Count > 0)
                {
                    App info = apps.FirstOrDefault<App>();
                    if (info != null && info.ID > 0)
                    {
                        string apkFileName = Utils.CleanFileName(info.Title);
                        string apkUrl = "http://1button.info/downloads/" + info.Code + "/" + apkFileName + ".apk";
                        try
                        {
                            status = 1; // processing
                            Utils.FileLog("Execution started for App '" + info.Title + "'");

                            Process p = new Process();
                            p.StartInfo.UseShellExecute = false;
                            p.StartInfo.CreateNoWindow = true;
                            p.StartInfo.RedirectStandardOutput = true;
                            p.StartInfo.FileName = ConfigurationSettings.AppSettings["BacthFilePath"];
                            p.StartInfo.WorkingDirectory = ConfigurationSettings.AppSettings["BatchFileHomeDir"];
                            p.StartInfo.Arguments = apkFileName + " " + info.Code + " " + info.CameraUrl + " " + info.CameraUser + " " + info.CameraPassword;
                            p.Start();
                            //p.WaitForExit();
                            // instead of p.WaitForExit(), do
                            StringBuilder q = new StringBuilder();
                            while (!p.HasExited)
                            {
                                q.Append(p.StandardOutput.ReadToEnd());
                            }
                            string r = q.ToString();

                            System.Threading.Thread.Sleep(100);

                            string apkFile = ConfigurationSettings.AppSettings["DownloadFilePath"] + info.Code + "\\" + apkFileName + ".apk";
                            if (File.Exists(apkFile))
                            {
                                SendEmail(info, apkFile, apkUrl);

                                status = 2; // completed
                                Utils.FileLog("Execution finished for App '" + info.Title + "'");
                            }
                            else
                            {
                                apkUrl = "";
                                status = 3; // failed
                                Utils.FileLog("Execution failed for App '" + info.Title + "'" + Environment.NewLine + r);
                            }
                        }
                        catch (Exception x)
                        {
                            apkUrl = "";
                            status = 3; // failed
                            Utils.FileLog(x.Message + Environment.NewLine + x.ToString());
                        }

                        if (AppDao.UpdateStatus(info.Code, status, apkUrl))
                            Utils.FileLog("App status successfully updated '" + info.Title + "' status (" + ((AppStatus)status).ToString() + ")" + Environment.NewLine);
                        else
                            Utils.FileLog("App status update failed '" + info.Title + "' status (" + ((AppStatus)status).ToString() + ")" + Environment.NewLine);
                    }
                    else
                    {
                        Utils.FileLog("No pending App found" + Environment.NewLine);
                    }
                }
                if (status == 0)    // i.e. no app processed
                    Thread.Sleep(int.Parse(ConfigurationSettings.AppSettings["CheckNewAppInterval"]));
            }
        }

        public void SendEmail(App app, string appFile, string apkUrl)
        {
            

            if (string.IsNullOrEmpty(app.Email))
            {
                Utils.FileLog("No email sent for App '" + app.Title + "'");
                return;
            }

            Utils.FileLog("Sending email for App '" + app.Title + "' to '" + app.Email + "'");
            string subject = Settings.EmailSubject + " - " + app.Title;
            string body = "Please find the attached Android App '" + Utils.CleanFileName(app.Title) + ".apk'. <br /><br /> <img src='" + app.ThumbFile + "'> <br />" +
                "<br />or <a href='" + apkUrl + "'>download here</a>.";
            try
            {
                Utils.FileLog("Email attachment: " + appFile);
                List<string> attachments = new List<string>() { appFile };
                Utils.SendMail(subject, body, app.Email, attachments);
            }
            catch (Exception x)
            {
                Utils.FileLog("Error sending email for App '" + app.Title + "' to '" + app.Email + "'" + Environment.NewLine + x.ToString());
            }
        }

        public void StopExecution()
        {
            isServiceRunning = false;
        }
    }
}
