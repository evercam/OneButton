﻿using System;
using System.Linq;
using System.Configuration;

namespace BLL.Common
{
    public class Settings
    {
        public static string OneButtonClientName
        {
            get { return ConfigurationSettings.AppSettings["OneButtonClientName"]; }
        }

        public static string OneButtonClientID
        {
            get { return ConfigurationSettings.AppSettings["OneButtonClientID"]; }
        }

        public static string OneButtonClientSecret
        {
            get { return ConfigurationSettings.AppSettings["OneButtonClientSecret"]; }
        }

        public static string OneButtonClientUri
        {
            get { return ConfigurationSettings.AppSettings["OneButtonClientUri"]; }
        }

        public static string OneButtonAPIUrl
        {
            get { return ConfigurationSettings.AppSettings["OneButtonAPIUrl"]; }
        }

        public static string ConnectionString
        {
            get { return ConfigurationSettings.AppSettings["ConnectionString"]; }
        }

        public static string AppFilePath
        {
            get { return ConfigurationSettings.AppSettings["AppFilePath"]; }
        }

        public static string DownloadFilePath
        {
            get { return ConfigurationSettings.AppSettings["DownloadFilePath"]; }
        }

        public static string DevelopersList
        {
            get { return ConfigurationSettings.AppSettings["DevelopersList"]; }
        }

        public static string EmailSubject
        {
            get { return ConfigurationSettings.AppSettings["EmailSubject"]; }
        }

        public static string EmailSource
        {
            get { return ConfigurationSettings.AppSettings["EmailSource"]; }
        }

        public static string SmtpEmail
        {
            get { return ConfigurationSettings.AppSettings["SmtpEmail"]; }
        }

        public static string SmtpUser
        {
            get { return ConfigurationSettings.AppSettings["SmtpUser"]; }
        }

        public static string SmtpPassword
        {
            get { return ConfigurationSettings.AppSettings["SmtpPassword"]; }
        }

        public static string SmtpServer
        {
            get { return ConfigurationSettings.AppSettings["SmtpServer"]; }
        }

        public static string SmtpServerPort
        {
            get { return ConfigurationSettings.AppSettings["SmtpServerPort"]; }
        }

        public static string ExceptionFromEmail
        {
            get { return ConfigurationSettings.AppSettings["ExceptionFromEmail"]; }
        }

        public static int TimeoutLimit
        {
            get { return int.Parse(ConfigurationSettings.AppSettings["TimeoutLimit"]); }
        }

        public static int RetryInterval
        {
            get { return int.Parse(ConfigurationSettings.AppSettings["RetryInterval"]); }
        }

        public static int RecheckInterval
        {
            get { return int.Parse(ConfigurationSettings.AppSettings["RecheckInterval"]); }
        }
    }
}
