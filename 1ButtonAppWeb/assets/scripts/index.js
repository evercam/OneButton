var Index = function () {
    var isLogin = false;
    var createUserState = false;
    var exitRegister = false;
    var EvercamApi = "https://api.evercam.io/v1";
    var appApiUrl = "http://1button.info/v1";
    var utilsApi = "http://1button.info/v1";
    var appsCount = 1;
    var isFirst = true;
    var retries = 1;
    
    var getParameterByName = function (name, searchString) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(searchString);//location.search);
        return results == null ? null : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    var getQueryStringByName = function (name) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results == null ? null : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    function toTitleCase(str) {
        //return str.replace(/\w\S*/g, function (txt) { return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); });
        return str.charAt(0).toUpperCase() + str.substr(1);
    }

    function autoLogIn(code) {
        var form = document.createElement("form");
        var element1 = document.createElement("input");
        var element2 = document.createElement("input");
        var element3 = document.createElement("input");
        var element4 = document.createElement("input");
        var element5 = document.createElement("input");

        form.method = "POST";
        form.action = "https://dashboard.evercam.io/oauth2/token";

        element1.value = code;
        element1.name = "code";
        form.appendChild(element1);

        element2.value = c4203c3e;
        element2.name = "client_id";
        form.appendChild(element2);

        element2.value = "55e2df6518ec146e0d968d640064017d";
        element2.name = "client_secret";
        form.appendChild(element2);

        element2.value = "http://astimegoes.by";
        element2.name = "redirect_uri";
        form.appendChild(element2);

        element2.value = "authorization_code";
        element2.name = "grant_type";
        form.appendChild(element2);

        document.body.appendChild(form);

        form.submit();
    }

    var format = function (state) {
        if (!state.id) return state.text; // optgroup
        return "<img class='flag' src='assets/img/flags/" + state.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + state.text;
    }
    
    var handleLoginSection = function () {
        if (sessionStorage.getItem("oAuthToken") != null && sessionStorage.getItem("oAuthToken") != undefined) {
            getMyApps();
            getUsersInfo();
        }
        else {
            $("#divApps").html('');
            $(".fullwidthbanner-container").show();
                
            var stringHash = window.location.hash;
            if (stringHash != "") {
                var hasToken = getParameterByName('access_token', "?" + stringHash.substring(1));
                if (hasToken != "" && hasToken != null) {
                    var hasTokenType = getParameterByName('token_type', window.location.hash);
                    var tokenExpiry = getParameterByName('expires_in', window.location.hash);
                    $.ajax({
                        type: 'POST',
                        url: utilsApi + '/tokeninfo',
                        data: { token_endpoint: "https://api.evercam.io/oauth2/tokeninfo?access_token=" + hasToken },
                        dataType: 'json',
                        ContentType: 'application/json',
                        success: function (res) {
                            if (res.userid != "null" && res.userid != '') {
                                var userId = res.userid;
                                sessionStorage.setItem("oAuthTokenType", hasTokenType);
                                sessionStorage.setItem("oAuthToken", hasToken);
                                sessionStorage.setItem("tokenExpiry", tokenExpiry);
                                sessionStorage.setItem("appUserId", userId)
                                sessionStorage.setItem("AppUsername", userId);
                                window.location.hash = '';
                                getMyApps();
                                getUsersInfo();
                            } else window.location = 'login.html';
                        },
                        error: function (xhr, textStatus) {

                        }
                    });
                }
                else
                    window.location = 'login.html';
            }
            else 
                window.location = 'login.html';
            
            $("#liUsername").hide();
            $("#lnkSignout").hide();
            $(".AppsContainer").hide();
            $(".responsivenav").removeClass("btn");
            $(".responsivenav").removeClass("btn-navbar");
        }
    }

    var clearPage = function () {
        $("#liUsername").show();
        $("#lnkSignout").show();
        $("#btnNewApp").show();
        $("#divMainContainer").removeClass("container-bg");

        $("#newApp").html("");
        $("#newApp").fadeOut();
        
    }

    var handleLogout = function () {
        $("#lnkLogout").bind("click", function () {
            sessionStorage.removeItem("oAuthToken");
            sessionStorage.removeItem("AppUsername"); 
            sessionStorage.removeItem("appUserId");
            sessionStorage.removeItem("oAuthTokenType");
            sessionStorage.removeItem("timelapseUserId");
            sessionStorage.removeItem("loggedInUserEmail");
            localStorage.removeItem("EvercamCameras");
            localStorage.removeItem("sharedcameras");
            window.location = 'login.html';            
        });
    }

    var handleNewApp = function () {
        $("#lnNewApp").bind("click", function () {
            showNewAppForm();
        });

        $("#lnNewAppCol").bind("click", function () {
            $("#newApp").slideUp(800, function () {
                $("#newApp").html("");
                $("#lnNewApp").show();
                $("#lnNewAppCol").hide();
            });

            ApiAction = 'POST';
            $("#txtCameraCode0").val('');
        });

        $("#lnNewCamera").bind("click", function () {
            showNewCameraForm();
        });
        $("#lnNewCameraCol").bind("click", function () {
            $("#newApp").slideUp(800, function () {
                $("#newApp").html("");
                $("#lnNewCamera").show();
                $("#lnNewCameraCol").hide();
            });
        });
    }

    var showNewCameraForm = function () {
        $.get('NewCameraForm.html', function (data) {
            $("#newApp").html(data);
            $("#lnNewCamera").hide();
            $("#lnNewCameraCol").show();
            $("#newApp").slideDown(800);
        });
    }

    $(".newApp").live("click", function () {
        showNewAppForm();
        $("#divLoadingApps").fadeOut();
    })

    var showNewAppForm = function () {
        $.get('NewApp.html', function (data) {
            $("#newApp").html(data);
            $("#lnNewApp").hide();
            $("#lnNewAppCol").show();
            $("#txtEmail").val(sessionStorage.getItem("loggedInUserEmail"));
            getCameras(true);
            $('#txtEmail').tagsInput({
                defaultText: 'Add Email',
                //width: '100%',
                height: 'auto',
                'onAddTag': function (email) {
                    if (!validateEmailByVal(email)) {
                        $("#divAlert").slideDown();
                        $("#divAlert span").html("Invalid email.");
                    }
                },
            });
            $("#newApp").slideDown(800);
            handleFileupload();
        });
    }

    var handleMyApps = function () {
        $("#lnMyTimelapse").bind("click", function () {
            getMyApps();
        });
    }

    var clearNewAppForm = function () {
        $("#ddlCameras").val(0);
        $("#txtTitle").val('');
        $("#txtEmail").val('');
        $("#chkCamSnap").attr('checked', 'checked');
        $('.table-striped tbody').html("");
        $("#divChooseFile").slideUp();
    }

    var checkFileUploadAndSave = function (percentage) {
        $("#divAlert").removeClass("alert-error").addClass("alert-info");
        $("#divAlert").slideDown();
        $("#divAlert span").html('<img src="assets/img/loader3.gif"/>&nbsp;Saving app');

        if (percentage != null && parseInt(percentage) <= 100) {
            setTimeout(function () { checkFileUploadAndSave($(".progress-success").attr("aria-valuemin")); }, 1000);
            return;
        }
        var iconFile = '';
        if ($("#chkChoose").is(":checked"))
            iconFile = $('.table-striped td.preview a').attr("href");

        var camUrl = '';
        var camUsername = '';
        var camPass = '';
        var isFound = false;
        var cams = JSON.parse(localStorage.getItem("EvercamCameras"));
        for (var i = 0; i < cams.cameras.length; i++) {
            if (cams.cameras[i].id == $("#ddlCameras").val()) {
                camUsername = cams.cameras[i].cam_username;
                camPass = cams.cameras[i].cam_password;
                camUrl = cams.cameras[i].external.http.jpg;//"http://" + cams.cameras[i].external.http.jpg + ":" + cams.cameras[i].external_http_port + cams.cameras[i].jpg_url;
                isFound = true;
            }
        }
        if (!isFound) {
            cams = JSON.parse(localStorage.getItem("sharedcameras"));
            if (cams != null && cams != undefined) {
                for (var i = 0; i < cams.cameras.length; i++) {
                    if (cams.cameras[i].id == $("#ddlCameras").val()) {
                        camUsername = cams.cameras[i].cam_username;
                        camPass = cams.cameras[i].cam_password;
                        camUrl = cams.cameras[i].external.http.jpg;//"http://" + cams.cameras[i].external.http.jpg + ":" + cams.cameras[i].external_http_port + cams.cameras[i].jpg_url;
                        isFound = true;
                    }
                }
            }
        }

        var o = {
            "camera_id"      : $("#ddlCameras").val(),
            "camera_url"     : camUrl,
            "camera_user"    : camUsername,
            "camera_password": camPass,
            "user_id"        : sessionStorage.getItem("appUserId"),
            "title"          : $("#txtTitle").val(),
            "email"          : $(".txtEmailBox").val(),
            "image_file"     : iconFile
        };
        
        $.ajax({
            type: 'POST',
            url: appApiUrl + '/apps',
            data: o,
            dataType: 'json',
            ContentType: "application/x-www-form-urlencoded",//'application/json; charset=utf-8',
            beforeSend: function (xhrObj) {
                xhrObj.setRequestHeader("Authorization", "Basic " + sessionStorage.getItem("oAuthToken"));
            },
            success: function (data) {
                appsCount = 1;
                /*var html = '';
                if (appsCount == 6) {
                    html += getHtml(data, 'app-box-size');
                    appsCount = 1;
                } else {
                    html += getHtml(data, 'app-box-size-margin');
                    appsCount++;
                }*/
                getMyApps(true);
                setTimeout(function () { CheckAppStatus(data.code); }, 30000);
                $("#newApp").slideUp(1000, function () {
                    //$("#divApps").prepend(html);
                    /*$('#divShare' + data.code).popbox();
                    $('#divDelete' + data.code).popbox({
                        open: '.open2',
                        box: '.box2',
                        arrow: '.arrow2',
                        arrow_border: '.arrow-border2',
                        close: '.closepopup2'
                    });*/
                    $("#newApp").html("");
                    $("#lnNewApp").show();
                    $("#lnNewAppCol").hide();
                    //$("#divLoadingApps").hide();
                });
                
            },
            error: function (xhr, textStatus) {
                $("#divAlert").removeClass("alert-info").addClass("alert-error");
                $("#divAlert span").html('App could not be saved.');
            }
        });
    };

    var CheckAppStatus = function (appCode) {
        $.ajax({
            type: 'GET',
            url: appApiUrl + '/apps/' + appCode,
            dataType: 'json',
            ContentType: 'application/json',
            success: function (res) {
                if (retries < 5 && (res.status == 0 || res.status == 1 || res.status == 3)) {
                    setTimeout(function () { CheckAppStatus(appCode); }, 10000);
                    retries++;
                }
                else if (retries > 5 && (res.status == 0 || res.status == 1 || res.status == 3)) {
                    $("#appImg" + appCode).removeClass("app-img-radious").addClass("app-img-radious-retry");
                    $("#appImg" + appCode).attr("src", "assets/img/retry.png");

                }
                else if (res.status == 2) {
                    if (res.app_url) {
                        $("#lnkDownload" + appCode).removeClass("link-disable").addClass("link-down");
                        $("#lnkDownload" + appCode).attr("href", res.app_url);
                        $("#lnkDownload" + appCode).attr("title", "Download .apk");
                        $("#lnkShare" + appCode).removeClass("link-disable").addClass("link-down").addClass("open");
                        $("#lnkShare" + appCode).attr("title", "Share .apk with friend");
                        $('#tile' + appCode).effect("pulsate");
                    }
                }
                else {

                }
            },
            error: function (xhr, textStatus) {

            }
        });
    };
    
    $(".formButtonCancel").live("click", function () {
        $("#newApp").slideUp(800, function () {
            $("#newApp").html("");
            $("#lnNewApp").show();
            $("#lnNewAppCol").hide();
            if ($("#divApps").html() == "") {
                $("#divApps").html('');
                $("#divLoadingApps").html('You have not created any app. <a href="javascript:;" class="newApp">Click</a> to create one.');
                $("#divLoadingApps").slideDown();
            }
        });
    });

    $(".formButtonOk").live("click", function () {
        
        $("#divAlert").removeClass("alert-info").addClass("alert-error");
        if ($("#ddlCameras").val() == "0") {
            $("#divAlert").slideDown();
            $("#divAlert span").html("Please select camera to continue.");
            return;
        }
        if ($("#txtTitle").val() == '') {
            $("#divAlert").slideDown();
            $("#divAlert span").html("Please enter app title.");
            return;
        }
        if ($("#txtEmail").val() != '') {
            var emails = $(".txtEmailBox").val().split(",");
            for (var i = 0; i < emails.length; i++) {
                if (!validateEmailByVal(emails[i])) {
                    $("#divAlert").slideDown();
                    $("#divAlert span").html("Invalid email '" + emails[i] + "'.");
                    return;
                }
            }
        }
        if ($("#chkChoose").is(":checked")) {
            if ($('.table-striped td.name').html() == undefined) {
                $("#divAlert").slideDown();
                $("#divAlert span").html("Please choose an icon to upload.");
                return;
            }
            $(".fileupload-buttonbar").hide();
            $("#divAlert").removeClass("alert-error").addClass("alert-info");
            $("#divAlert").slideDown();
            $("#divAlert span").html('<img src="assets/img/loader3.gif"/>&nbsp;Saving app');
            $('.table-striped td.start button.btn').click();
            setTimeout(function () { checkFileUploadAndSave($(".progress-success").attr("aria-valuemin")); }, 1000);
        } else
            checkFileUploadAndSave(null);
    });

    $('[name="AppIcon"]').live("click", function () {
        var id = $(this).attr("id");
        if (id == "chkChoose") {
            $("#divChooseFile").slideDown();
        }
        else
            $("#divChooseFile").slideUp();
    });

    $(".cameraformButtonOk").live("click", function () {
        var caneraSnaps;
        $("#divAlert0").removeClass("alert-info").addClass("alert-error");
        if ($("#txtCameraUniqueId").val() == "") {
            $("#divAlert0").slideDown();
            $("#divAlert0 span").html("Please enter unique camera ID.");
            return;
        }
        if ($("#txtCameraName").val() == '') {
            $("#divAlert0").slideDown();
            $("#divAlert0 span").html("Please enter camera name.");
            return;
        }
        if ($("#txtCameraEndpoints").val() == "") {
            $("#divAlert0").slideDown();
            $("#divAlert0 span").html("Please enter camera endpoint.");
            return;
        }
        if ($("#txtCameraUsername").val() != "" && $("#txtCameraPassword").val() == "") {
            $("#divAlert0").slideDown();
            $("#divAlert0 span").html("Please enter camera Password.");
            return;
        }
        if ($("#txtCameraUsername").val() == "" && $("#txtCameraPassword").val() != "") {
            $("#divAlert0").slideDown();
            $("#divAlert0 span").html("Please enter camera Username.");
            return;
        }
        
        var o = {
            "id": $("#txtCameraUniqueId").val(),
            "name": $("#txtCameraName").val(),
            "endpoints": $("#txtCameraEndpoints").val(),
            "is_public": $("#rdPublicCamera").is(":checked")
        };

        if ($("#txtCameraUrl").val() != "") 
            o.snapshots = { 'jpg': $("#txtCameraUrl").val() };
        if ($("#txtCameraUsername").val() != "" && $("#txtCameraPassword").val() != "")
            o.auth = {'basic': {'username': 'user1', 'password': 'abcde'}};//CryptoJS.SHA1($("#txtCameraUsername").val() + ":" + $("#txtCameraPassword").val());

        $("#divAlert0").removeClass("alert-error").addClass("alert-info");
        $("#divAlert0").slideDown();
        $("#divAlert0 span").html('<img src="assets/img/loader3.gif"/>&nbsp;Saving Camera');
        //Evercam.setBasicAuth("", "", sessionStorage.getItem("oAuthToken"));
        //Evercam.Camera.create(o);
        $.ajax({
            type: "POST",
            url: EvercamApi + "/cameras ",
            data: o,
            dataType: 'json',
            ContentType: "application/x-www-form-urlencoded",//'application/json; charset=utf-8',
            beforeSend: function (xhrObj) {
                xhrObj.setRequestHeader("Authorization", sessionStorage.getItem("oAuthTokenType") + " " + sessionStorage.getItem("oAuthToken"));
            },
            success: function (data) {
                $("#divAlert0 span").html('Camera saved.');
                $("#txtCameraUniqueId").val('');
                $("#txtCameraName").val('');
                $("#txtCameraEndpoints").val('');
                $("#txtCameraUrl").val('');
                $("#txtCameraUsername").val('');
                $("#txtCameraPassword").val('');
                setTimeout(function () {
                    $("#newApp").slideUp(800, function () { $("#newApp").html(""); });
                    $("#divAlert0").slideUp();
                }, 6000);
            },
            error: function (xhr, textStatus) {
                $("#divAlert0").removeClass("alert-info").addClass("alert-error");
                $("#divAlert0 span").html(xhr.responseJSON.message);
            }
        });
    });
    
    var getMyApps = function (refresh) {
        $("#liUsername").show();
        $("#lnkSignout").show();
        $("#btnNewApp").show();
        $("#divMainContainer").removeClass("container-bg");

        $("#newApp").html("");
        $("#newApp").fadeOut();

        $("#displayUsername").html(sessionStorage.getItem("AppUsername"));
        
        if (!refresh) {
            $("#divLoadingApps").fadeIn();
            $("#divLoadingApps").html('<img src="assets/img/loader3.gif" alt="Loading..."/>&nbsp;Fetching Apps');
        }
        /********************************************/
        
        $.ajax({
            type: 'GET',
            url: appApiUrl + "/apps/users/" + sessionStorage.getItem("appUserId"),
            dataType: 'json',
            ContentType: 'application/json; charset=utf-8',
            /*beforeSend: function (xhrObj) {
                xhrObj.setRequestHeader("Authorization", "Basic " + sessionStorage.getItem("oAuthToken"));
            },*/
            success: function (data) {
                if (data.length == 0) {
                    $("#divApps").html('');
                    $("#divLoadingApps").html('You have not created any apps. <a href="javascript:;" class="newApp">Click</a> to create one.');
                }
                else {
                    var html = '';
                    for (var i = 0; i < data.length; i++) {
                        if (appsCount == 6) {
                            html += getHtml(data[i], 'app-box-size');
                            appsCount = 1;
                        } else {
                            html += getHtml(data[i], 'app-box-size-margin');
                            appsCount++;
                        }
                    }
                    $("#divApps").html(html);
                    $(".AppsContainer").fadeIn();
                    $("#divLoadingApps").fadeOut();
                    $('.popbox').popbox();
                    $('.popbox2').popbox({
                        open: '.open2',
                        box: '.box2',
                        arrow: '.arrow2',
                        arrow_border: '.arrow-border2',
                        close: '.closepopup2'
                    });
                }
            },
            error: function (xhr, textStatus) {
                $("#divApps").html('');
                //$("#divLoadingApps").show();
                $("#divLoadingApps").html('You have not created any app. <a href="#" class="newApp">Click</a> to create one.');
            }
        });
    }

    var getHtml = function (data, divClass, singleTile) {
        var thumbImage = data.thumb_file;
        if (!data.thumb_file)
            thumbImage = "../assets/img/noimagegray.jpg";
        
        var html = '<div id="tile' + data.code + '" class="' + divClass + '">';
        html += '       <div class="app-box padding14">';
        html += '          <div id="divThumb' + data.code + '" class="row-fluid text-center" style="height:90px;">';
        if (data.status != 3)
            html += '             <img id="appImg' + data.code + '" style="background: url(' + thumbImage + ') no-repeat;" class="app-img-radious" data-val="' + data.code + '" src="' + thumbImage + '" />';
        else
            html += '             <img id="appImg' + data.code + '" style="background: url(' + thumbImage + ') no-repeat;" class="app-img-radious-retry" data-val="' + data.code + '" src="assets/img/retry.png" />';
        html += '          </div>';

        html += '           <br />';
        html += '          <div id="divTitle' + data.code + '" title="' + data.title + '" class="row-fluid app-title" style="font-weight:bold;">';
        html += data.title;
        html += '             <span class="paragraph-end"></span>';
        html += '          </div>';

        html += '          <div id="divAppUrl' + data.code + '" class="row-fluid">';

        if (data.app_url)
            html += '             <a id="lnkDownload' + data.code + '" href="' + data.app_url + '" target="_blank" class="link-down"  data-action="d">Download .apk</a>';
        else
            html += '             <a id="lnkDownload' + data.code + '" href="javascript:;" target="_blank" class="link-disable" data-action="d" title="Please wait...">Download .apk</a>';

        html += '          </div>';

        html += '          <div id="divShare' + data.code + '" class="row-fluid popbox">';

        if (data.app_url)
            html += '             <a id="lnkShare' + data.code + '" href="javascript:;" class="link-down open"  data-action="s">Share with Friends</a>';
        else
            html += '             <a id="lnkShare' + data.code + '" href="javascript:;" class="link-disable open" data-action="s" title="Please wait...">Share with Friends</a>';

        html += '             <div class="collapse-popup">';
        html += '               <div class="box" style="width:237px;">';
        html += '                   <div class="arrow"></div>';
        html += '                   <div class="arrow-border"></div>';
        html += '                   <form action="" method="post" id="subForm">';
        html += '                       <div id="divShareBox' + data.code + '"><div class="input">';
        html += '                           <input type="text" name="txtShareEmail' + data.code + '" id="txtShareEmail' + data.code + '" placeholder="Email" />';
        html += '                       </div>';
        html += '                       <div class="margin-bottom10"><textarea id="txtEmailMsg' + data.code + '" cols="32" rows="3"></textarea></div>';
        html += '                       <input class="btn shareWithFriend" type="button" value=" Share " data-val="' + data.code + '" />&nbsp;&nbsp;<a href="#" class="closepopup">Cancel</a></div>';
        html += '                       <div id="divMailSuccess' + data.code + '" class="text-center hide"></div>';
        html += '                   </form>';
        html += '               </div>';
        html += '             </div>';
        html += '           </div>';

        html += '          <div id="divDelete' + data.code + '" class="row-fluid popbox2">';
        html += '             <a href="javascript:;" data-val="' + data.code + '" class="link-down open2" data-action="rr">Delete App</a>';
        html += '             <div class="collapse-popup">';
        html += '               <div class="box2" style="width:237px;">';
        html += '                   <div class="arrow2"></div>';
        html += '                   <div class="arrow-border2"></div>';
        html += '                   <div class="margin-bottom-10">Are you sure to delete "' + data.title + '"?</div>';
        html += '                   <div class="margin-bottom-10"><input class="btn link-down" type="button" value=" Delete " data-val="' + data.code + '" data-action="r" />&nbsp;&nbsp;<a href="#" class="closepopup2">Cancel</a></div>';
        html += '               </div>';
        html += '             </div>';
        html += '          </div>';

        html += '          </div>';
        html += '       </div>';
        html += '    </div>';
        //***********************************************************************************************************
        
        return html;
    }

    $(".app-img-radious-retry").live("click", function () {
        retries = 1;
        var oImg = $(this);
        var code = oImg.attr("data-val");
        oImg.attr("src", "assets/img/retry-app.gif");
        $.ajax({
            type: "PUT",
            url: appApiUrl + "/apps/" + code+"/0",
            //data: { "status": 0, "app_url": "" },
            contentType: "application/x-www-form-urlencoded",
            dataType: "json",
            success: function (data) {
                var thumbImage = data.thumb_file;
                if (!data.thumb_file)
                    thumbImage = "../assets/img/noimagegray.jpg";
                oImg.removeClass("app-img-radious-retry").addClass("app-img-radious");
                oImg.attr("src", thumbImage);
                setTimeout(function () { CheckAppStatus(data.code); }, 30000);
                retries = 2;
            },
            error: function (xhrc, ajaxOptionsc, thrownErrorc) { oImg.attr("src", "assets/img/retry.png"); }
        });
    });

    $(".shareWithFriend").live("click", function () {
        var btn = $(this);
        btn.attr("value", " Sending... ");
        btn.attr("disabled", "disabled");
        var code = btn.attr("data-val");
        var msg = $("#txtEmailMsg" + code).val();
        var title = $("#divTitle" + code).attr("title");
        var o = {
            "from": sessionStorage.getItem("AppUsername"),
            "subject": sessionStorage.getItem("AppUsername") + " has shared 1ButtonApp '" + title + "' with you",
            "message": msg + '<br /><br /><img src="' + $("#divThumb" + code + " img").attr("src") + '"><br><br>Click <a href="' + $("#divAppUrl" + code + " a").attr("href") + '">here</a> to Download',
            "recipients": $("#txtShareEmail" + code).val(),
            "ccs": "",
            "bccs": ""
        };
        $.ajax({
            type: 'POST',
            url: utilsApi + "/email",
            data: o,
            dataType: 'json',
            ContentType: 'application/x-www-form-urlencoded',
            success: function (data) {
                $("#txtShareEmail" + code).val("");
                $("#txtEmailMsg" + code).val("");
                btn.attr("value", " Share ");
                btn.removeAttr("disabled");
                $("#divShareBox" + code).slideUp();
                $("#divMailSuccess" + code).css("color", "green");
                $("#divMailSuccess" + code).html("Mail sent successfully.");
                $("#divMailSuccess" + code).slideDown();
                setTimeout(function () {
                    $("#divMailSuccess" + code).slideUp();
                    $("#divShareBox" + code).slideDown();
                }, 8000);
            },
            error: function (xhr, textStatus) {
                btn.attr("value", " Share ");
                btn.removeAttr("disabled");
                $("#divMailSuccess" + code).css("color", "red");
                $("#divMailSuccess" + code).html("Failed to send Mail.");
                $("#divMailSuccess" + code).slideDown();
                setTimeout(function () {
                    $("#divMailSuccess" + code).slideUp();
                }, 8000);
            }
        });
    });
    
    var getCameras = function (reload) {
        $.ajax({
            type: "GET",
            crossDomain: true,
            xhrFields: {
                withCredentials: true
            },
            url: EvercamApi + "/users/" + sessionStorage.getItem("appUserId") + "/cameras",
            data: { include_shared: true, thumbnail: false },
            beforeSend: function (xhrObj) {
                xhrObj.setRequestHeader("Authorization", sessionStorage.getItem("oAuthTokenType") + " " + sessionStorage.getItem("oAuthToken"));
            },
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (res) {
                localStorage.setItem("EvercamCameras", JSON.stringify(res));
                bindDropDown(reload);
            },
            error: function (xhrc, ajaxOptionsc, thrownErrorc) {
                if (xhrc.status == 401)
                {
                    sessionStorage.removeItem("oAuthToken");
                    sessionStorage.removeItem("AppUsername");
                    sessionStorage.removeItem("appUserId");
                    sessionStorage.removeItem("oAuthTokenType");
                    sessionStorage.removeItem("timelapseUserId");
                    localStorage.removeItem("EvercamCameras");
                    window.location = 'login.html';
                }
            }
        });
    }

    var bindDropDown = function (reload) {
        if (reload) {
            var cams = JSON.parse(localStorage.getItem("EvercamCameras"));
            for (var i = 0; i < cams.cameras.length; i++) {
                var css = 'onlinec';
                if (!cams.cameras[i].is_online)
                    css = 'offlinec';
                if (cams.cameras[i].thumbnail_url != undefined)
                    $("#ddlCameras").append('<option class="' + css + '" data-val="' + cams.cameras[i].thumbnail_url + '" value="' + cams.cameras[i].id + '" >' + cams.cameras[i].name + '</option>');
            }
            $("#imgCamLoader").hide();
            $("#ddlCameras").select2({
                placeholder: 'Select Camera',
                allowClear: true,
                formatResult: format,
                formatSelection: format,
                escapeMarkup: function (m) {
                    return m;
                }
            });
        } else
            getMyApps();
    }

    var format = function (state) {
        if (!state.id) return state.text;
        if (state.id == "0") return state.text;
        //return "<img class='flag' src='assets/img/" + state.css + ".png'/>&nbsp;&nbsp;" + state.text;
        if (state.element[0].attributes[1].nodeValue == "null")
            return "<table style='width:100%;'><tr><td style='width:90%;'><img style='width:35px;height:30px;' class='flag' src='assets/img/cam-img-small.jpg'/>&nbsp;&nbsp;" + state.text + "</td><td style='width:10%;' align='right'>" + "<img style='margin-top: -6px;' class='flag' src='assets/img/" + state.css + ".png'/>" + "</td></tr></table>";
        else
            return "<table style='width:100%;'><tr><td style='width:90%;'><img style='width:35px;height:30px;' class='flag' src='" + state.element[0].attributes[1].nodeValue + "'/>&nbsp;&nbsp;" + state.text + "</td><td style='width:10%;' align='right'>" + "<img style='margin-top: -6px;' class='flag' src='assets/img/" + state.css + ".png'/>" + "</td></tr></table>";
    }

    var getUsersInfo = function () {
        $.ajax({
            type: "GET",
            crossDomain: true,
            xhrFields: {
                withCredentials: true
            },
            url: EvercamApi + "/users/" + sessionStorage.getItem("appUserId") + ".json",
            //data: { api_id: "c298268b", api_key: "600736690c93646b89e3230480978629" },
            beforeSend: function (xhrObj) {
                xhrObj.setRequestHeader("Authorization", sessionStorage.getItem("oAuthTokenType") + " " + sessionStorage.getItem("oAuthToken"));
            },
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (res) {
                sessionStorage.setItem("loggedInUserEmail", res.users[0].email);
                if (res.users[0].firstname == "" && res.users[0].lastname == "")
                    return;
                sessionStorage.setItem("AppUsername", res.users[0].firstname + " " + res.users[0].lastname);
                $("#displayUsername").html(res.users[0].firstname + " " + res.users[0].lastname)
            },
            error: function (xhrc, ajaxOptionsc, thrownErrorc) { }
        });
    }
        
    $('.link-down').live('click', function (e) {
        var code = $(this).attr("data-val");
        var action = $(this).attr("data-action");
        
        if (action == 'r') {
            //jConfirm("Are you sure? ", "Delete .apk", function (result) {
            //if (result)
            RemoveApp(code);
            //});
        }
        else if (action == 'e') {
            if ($("#code" + code).css("display") == 'none')
                $("#code" + code).slideDown(1000);
            else
                $("#code" + code).slideUp(1000);
        }
        else if (action == 'd1') {
            SaveToDisk($(this).attr("data-url"), code);
        }
    });

    var SaveToDisk = function (fileURL, fileName) {
        // for non-IE
        if (!window.ActiveXObject) {
            var save = document.createElement('a');
            save.href = fileURL;
            save.target = '_blank';
            save.download = fileName || 'unknown';

            var event = document.createEvent('Event');
            event.initEvent('click', true, true);
            save.dispatchEvent(event);
            (window.URL || window.webkitURL).revokeObjectURL(save.href);
        }
            // for IE
        else if (!!window.ActiveXObject && document.execCommand) {
            var _window = window.open(fileURL, '_blank');
            _window.document.close();
            _window.document.execCommand('SaveAs', true, fileName || fileURL)
            _window.close();
        }
    }

    var RemoveApp = function (code) {
        $("#tile" + code).fadeOut(1000, function () {
            $.ajax({
                type: "DELETE",
                url: appApiUrl + "/apps/" + code,
                /*beforeSend: function (xhrObj) {
                    xhrObj.setRequestHeader("Authorization", "Basic " + sessionStorage.getItem("oAuthToken"));
                },*/
                contentType: "application/x-www-form-urlencoded",//"text/plain; charset=utf-8",
                dataType: "json",
                success: function (res) {
                    $("#tile" + code).remove();
                    if ($("#divApps div").length == 0) {
                        $("#divApps").html('');
                        $("#divLoadingApps").html('You have not created any app. <a href="javascript:;" class="newApp">Click</a> to create one.');
                        $("#divLoadingApps").slideDown();
                    }
                },
                error: function (xhrc, ajaxOptionsc, thrownErrorc) { }
            });
        });
    }
    
    var getUserLocalIp = function () {
        try{
            if (window.XMLHttpRequest) xmlhttp = new XMLHttpRequest();
            else xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

            xmlhttp.open("GET", "http://api.hostip.info/get_html.php", false);
            xmlhttp.send();

            hostipInfo = xmlhttp.responseText.split("\n");

            for (i = 0; hostipInfo.length >= i; i++) {
                var ipAddress = hostipInfo[i].split(":");
                if (ipAddress[0] == "IP") return $("#user_local_Ip").val(ipAddress[1]);
            }
        }
        catch(e){}
    }

    var handleFileupload = function () {
        
        // Initialize the jQuery File Upload widget:
        $('#fileupload').fileupload({
            // Uncomment the following to send cross-domain cookies:
            //xhrFields: {withCredentials: true},
            url: 'assets/plugins/jquery-file-upload/server/php/'
        });
        
        return;
        // Load existing files:
        // Demo settings:
        $.ajax({
            // Uncomment the following to send cross-domain cookies:
            //xhrFields: {withCredentials: true},
            url: $('#fileupload').fileupload('option', 'url'),
            dataType: 'json',
            context: $('#fileupload')[0],
            maxFileSize: 5000000,
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
            process: [{
                action: 'load',
                fileTypes: /^image\/(gif|jpeg|png)$/,
                maxFileSize: 20000000 // 20MB
            }, {
                action: 'resize',
                maxWidth: 1440,
                maxHeight: 900
            }, {
                action: 'save'
            }
            ]
        }).done(function (result) {
            $(this).fileupload('option', 'done')
                .call(this, null, {
                    result: result
                });
        });

        // Upload server status check for browsers with CORS support:
        if ($.support.cors) {
            $.ajax({
                url: 'assets/plugins/jquery-file-upload/server/php/',
                type: 'HEAD'
            }).fail(function () {
                $('<span class="alert alert-error"/>')
                    .text('Upload server currently unavailable - ' +
                    new Date())
                    .appendTo('#fileupload');
            });
        }
    }

    $("#ddlCameras").live("change", function () {
        var cameraId = $(this).val();
        //$("#imgCamStatus").hide();
        $("#imgPreview").hide();
        $("#imgPreviewLoader").show();
        $("#imgPreviewLoader").attr('src', 'assets/img/ajaxloader.gif');
        $.ajax({
            type: "GET",
            crossDomain: true,
            url: EvercamApi + "/cameras/" + cameraId + "/live.json",
            beforeSend: function (xhrObj) {
                xhrObj.setRequestHeader("Authorization", sessionStorage.getItem("oAuthTokenType") + " " + sessionStorage.getItem("oAuthToken"));
            },
            data: { with_data: true },
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (res) {
                if (res.data == null || res.data == undefined) {
                    $("#imgPreviewLoader").attr('src', 'assets/img/cam-img.jpg');
                } else {
                    $("#imgPreview").attr('src', res.data);
                    $("#imgPreview").show();
                    $("#imgPreviewLoader").hide();
                    $("#imgPreviewLoader").attr('src', 'assets/img/cam-img.jpg');
                }
            },
            error: function (xhrc, ajaxOptionsc, thrownErrorc) {
                $("#imgPreviewLoader").attr('src', 'assets/img/cam-img.jpg');
            }
        });
    })

    return {
        
        init: function () {
            //getUserLocalIp();
            //loadLogin();
            handleLoginSection();
            //handleWindowResize();
            handleNewApp();
            handleLogout();
            //handleMyApps();
            //handleFancyBox();
        }

    };
}();